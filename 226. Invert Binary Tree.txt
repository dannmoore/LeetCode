226. Invert Binary Tree

Invert a binary tree.

Example:

Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9

Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1

Trivia:
This problem was inspired by this original tweet by Max Howell:

    Google: 90% of our engineers use the software you wrote (Homebrew), but you can�t invert a binary tree on a whiteboard so f*** off.




/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    TreeNode* invertTree(TreeNode* root) {
         if (root != NULL) {
             // Recursively call left and right branches if possible
             invertTree(root->left);
             invertTree(root->right);

             // Swap left and right nodes
             TreeNode *p;
             p = root->left;
             root->left = root->right;
             root->right = p;
         }


         // Returning a value doesn't make sense as we're given a pointer, 
         // but the problem requires it...
         return(root);        
    }
};




According to leetcode, the memory footprint was smaller than 100% of other submissions.





