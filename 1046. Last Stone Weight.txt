1046. Last Stone Weight

We have a collection of rocks, each rock has a positive integer weight.

Each turn, we choose the two heaviest rocks and smash them together.  Suppose the stones have weights x and y with x <= y.  The result of this smash is:

    If x == y, both stones are totally destroyed;
    If x != y, the stone of weight x is totally destroyed, and the stone of weight y has new weight y-x.

At the end, there is at most 1 stone left.  Return the weight of this stone (or 0 if there are no stones left.)

 

Example 1:

Input: [2,7,4,1,8,1]
Output: 1
Explanation: 
We combine 7 and 8 to get 1 so the array converts to [2,4,1,1,1] then,
we combine 2 and 4 to get 2 so the array converts to [2,1,1,1] then,
we combine 2 and 1 to get 1 so the array converts to [1,1,1] then,
we combine 1 and 1 to get 0 so the array converts to [1] then that's the value of last stone.

 

Note:

    1 <= stones.length <= 30
    1 <= stones[i] <= 1000



class Solution {
public:
    int lastStoneWeight(vector<int>& stones) {
         // Process stones until there are zero or one stones left
         while (stones.size() > 1) {
             // Sort the stones
             std::sort(stones.begin(), stones.end());

             // If the stones are of equal weight, both are destroyed
             if (stones[stones.size() - 1] == stones[stones.size() - 2]) {
                 stones.pop_back();
                 stones.pop_back();
             }
             // Otherwise the resulting stone is the weight difference
             else {
                 stones[stones.size() - 2] = stones[stones.size() - 1] - stones[stones.size() - 2];
                 stones.pop_back();
             }
         }

         if (stones.size() == 1) return(stones[0]);
         else return(0);        
    }
};






According to leetcode, the memory footprint was smaller than 100% of other submissions.


