463. Island Perimeter

You are given a map in form of a two-dimensional integer grid where 1 represents land and 0 represents water.

Grid cells are connected horizontally/vertically (not diagonally). The grid is completely surrounded by water, and there is exactly one island (i.e., one or more connected land cells).

The island doesn't have "lakes" (water inside that isn't connected to the water around the island). One cell is a square with side length 1. The grid is rectangular, width and height don't exceed 100. Determine the perimeter of the island.

 

Example:

Input:
[[0,1,0,0],
 [1,1,1,0],
 [0,1,0,0],
 [1,1,0,0]]

Output: 16

Explanation: The perimeter is the 16 yellow stripes in the image below:




class Solution {
public:
    int islandPerimeter(vector<vector<int>>& grid) {
         int perimeter = 0;

         if (grid.size() == 0) return(0);	// Sanity check

         // Loop through each cell in the grid
         for (int y = 0; y < grid.size(); y++) {
             for (int x = 0; x < grid[y].size(); x++) {
                 if (grid[y][x] == 1) {	// Check for land
                     // Check north
                     if (y == 0) { // Edge of grid
                         perimeter++;
                     }
                     else if (grid[y - 1][x] == 0) {
                         perimeter++;
                     }

                     // Check south
                     if (y == grid.size() - 1) { // Edge of grid
                         perimeter++;
                     }
                     else if (grid[y + 1][x] == 0) {
                         perimeter++;
                     }

                     // Check east
                     if (x == grid[y].size() - 1) { // Edge of grid
                         perimeter++;
                     }
                     else if (grid[y][x + 1] == 0) {
                         perimeter++;
                     }


                     // Check west
                     if (x == 0) { // Edge of grid
                         perimeter++;
                     }
                     else if (grid[y][x - 1] == 0) {
                         perimeter++;
                     }
                 }
             }
         }

         return(perimeter);
    }
};





According to leetcode, the memory footprint was smaller than 100% of other submissions.
